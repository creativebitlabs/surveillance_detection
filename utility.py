import json
import io
import socket


def json_encode(obj, encoding="utf-8"):
    return json.dumps(obj, ensure_ascii=False).encode(encoding)


def json_decode(json_bytes, encoding="utf-8"):
    tiow = io.TextIOWrapper(
        io.BytesIO(json_bytes), encoding=encoding, newline=""
    )
    obj = json.load(tiow)
    tiow.close()
    return obj


def string_to_bool(value):
    return str(value).lower() in ["true", "t", "yes", "y", "1"]


def get_current_ip():
    sck = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # noinspection PyBroadException
    try:
        sck.connect(('10.255.255.255', 1))
        ip = sck.getsockname()[0]
    except:
        ip = '127.0.0.1'
    finally:
        sck.close()
    return ip
