from imageai.Detection import VideoObjectDetection
import os
import queue
import time
import cv2


class Analyzer:
    def __init__(self, config):
        self.execution_path: str = os.getcwd()
        self.config = config
        self.datasink: queue.Queue = queue.Queue()
        self.detector = None
        self.frame_function = lambda frame_number, output_array, output_count, returned_frame: ""
        self.full_function = lambda output_arrays, count_arrays, average_output_count: ""
        self.is_running: bool = True
        self.source_type = None
        self.current_infile: str = None
        self.current_out_file: str = None

    def stop(self):
        self.is_running = False

    def add_data(self, new_data):
        self.datasink.put(new_data)

    def get_config_value(self, param, default_value):
        try:
            return self.config["default"][param]
        except IndexError:
            return default_value

    def initialize(self, source_type):
        self.source_type = source_type
        if self.detector is None:
            self.detector = VideoObjectDetection()
            self.set_tf_model_type()
            self.detector.setModelPath(os.path.join(self.execution_path, self.get_config_value("tf_model", "")))
            self.detector.loadModel(detection_speed=self.get_config_value("tf_detection_speed", "normal"))

        print("initialization finished")

    def clear_current_in_out_source(self):
        self.current_out_file = None
        self.current_infile = None

    def set_on_frame(self, func):
        self.frame_function = func

    def set_on_finished(self, func):
        self.full_function = func

    def set_tf_model_type(self):
        tf_mdl_typ = self.get_config_value("tf_model_type", "")
        if tf_mdl_typ == "yolo":
            self.detector.setModelTypeAsYOLOv3()
            return
        elif tf_mdl_typ == "tiny_yolo":
            self.detector.setModelTypeAsTinyYOLOv3()
            return
        elif tf_mdl_typ == "retina_net":
            self.detector.setModelTypeAsRetinaNet()
            return

        raise ValueError("unknown tf_model_type parameter value")

    def analyze_file(self, video_input, video_output, frame_callback=None, finished_callback=None):
        print("analyze video : ", video_input)
        self.current_infile = video_input
        self.current_out_file = video_output

        custom_object = self.get_config_value("custom_object", "all")

        if custom_object == "" or custom_object == "all":
            video_path = self.detector.detectObjectsFromVideo(
                save_detected_video=True
                , input_file_path=video_input
                , output_file_path=video_output
                , frames_per_second=int(self.get_config_value("tf_frames_per_sec", "15"))
                , log_progress=True
                , minimum_percentage_probability=30
                , return_detected_frame=True
                , per_frame_function=frame_callback if frame_callback is not None else self.frame_function
                , video_complete_function=finished_callback if finished_callback is not None else self.full_function
            )
            print(video_path)
        elif custom_object == "no_detection":
            print("SUPRESS DETECTIONS : ", custom_object)
        else:
            persons = self.detector.CustomObjects(person=True)
            video_path = self.detector.detectCustomObjectsFromVideo(
                save_detected_video=True
                , input_file_path=video_input
                , output_file_path=video_output
                , frames_per_second=int(self.get_config_value("tf_frames_per_sec", "15"))
                , log_progress=True
                , minimum_percentage_probability=30
                , return_detected_frame=True
                , per_frame_function=frame_callback if frame_callback is not None else self.frame_function
                , video_complete_function=finished_callback if finished_callback is not None else self.full_function
                , custom_objects=persons
            )
            print(video_path)

    def analyze_stream(self, video_input, video_output, frame_callback=None, finished_callback=None):
        print("analyze video : ", video_input)
        self.current_infile = video_input
        self.current_out_file = video_output

        if "/" in video_input or "\\" in video_input or "." in video_input:
            camera = cv2.VideoCapture(video_input)
        else:
            camera = cv2.VideoCapture(int(video_input))

        custom_object = self.get_config_value("custom_object", "all")

        if custom_object == "" or custom_object == "all":
            video_path = self.detector.detectObjectsFromVideo(
                save_detected_video=True
                , camera_input=camera
                , output_file_path=video_output
                , frames_per_second=int(self.get_config_value("tf_frames_per_sec", "15"))
                , log_progress=True
                , minimum_percentage_probability=30
                , return_detected_frame=True
                , per_frame_function=frame_callback if frame_callback is not None else self.frame_function
                , video_complete_function=finished_callback if finished_callback is not None else self.full_function
            )
            print(video_path)
        elif custom_object == "no_detection":
            print("SUPRESS DETECTIONS : ", custom_object)
        else:
            persons = self.detector.CustomObjects(person=True)
            video_path = self.detector.detectCustomObjectsFromVideo(
                save_detected_video=True
                , camera_input=camera
                , output_file_path=video_output
                , frames_per_second=int(self.get_config_value("tf_frames_per_sec", "15"))
                , log_progress=True
                , minimum_percentage_probability=30
                , return_detected_frame=True
                , per_frame_function=frame_callback if frame_callback is not None else self.frame_function
                , video_complete_function=finished_callback if finished_callback is not None else self.full_function
                , custom_objects=persons
            )
            print(video_path)

    def start(self):
        if self.source_type == "file":
            while self.is_running:
                while not self.datasink.empty():
                    vin = self.datasink.get()
                    head, tail = os.path.split(vin)
                    vout = os.path.join(self.get_config_value("video_output_dst", self.execution_path),
                                        self.get_config_value("video_output_prefix", "re_") + tail)
                    print("input : ", vin)
                    print("output : ", vout)
                    self.analyze_file(vin, vout)
                time.sleep(1)
        elif self.source_type == "stream":
            # TODO : test and improve streaming
            in_vid = self.get_config_value("video_input_src", "0")
            out_vid = os.path.join(self.get_config_value("video_output_dst", self.execution_path),
                                   self.get_config_value("video_output_prefix", "re_") + "stream_")
            print(in_vid)
            print(out_vid)
            self.analyze_stream(in_vid, out_vid)
        else:
            print("invalid source type: ", self.source_type)

