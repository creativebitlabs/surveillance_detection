import unittest
import test_messages


def suite():
    __suite = unittest.TestSuite()
    __suite.addTests(test_messages.get_test_cases())

    # add other test cases here.

    return __suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
