import unittest
import messages as msg


class UTMessages(unittest.TestCase):
    def test_detect(self):
        test_msg = msg.Detect(data_source="my_test.mp4", data_source_type=msg.Detect.Type.FILE,
                              instance_id="my_test_instance", port_number=65300)
        js = test_msg.json_encode()
        test_msg_new = msg.Message.get_decode_message(js, decoded=False)
        self.assertEqual(test_msg_new.sender, test_msg.sender)

    def test_report(self):
        test_msg = msg.Report(instance_id="test_inst", input_name="mytest.mp4", output_name="mytest.mp4.avi")
        test_msg.add_detection("person", 1.25)
        test_msg.add_detection("car", 0.345)
        js = test_msg.json_encode()
        test_msg_new = msg.Message.get_decode_message(js, decoded=False)
        self.assertEqual(test_msg.input_name, test_msg_new.input_name)


def get_test_cases():
    return [UTMessages("test_detect"), UTMessages("test_report")]
