import sys
sys.path.insert(0, '../')

import net_server as netserver

server = netserver.NetServer("127.0.0.1", 65001)
server.start()
