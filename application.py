import configparser as cfg
import os
import time
from threading import Condition

from watchdog.events import PatternMatchingEventHandler
from watchdog.observers.polling import PollingObserver

import messages
import net_server as netserver
import utility
from ai_analyzer import Analyzer
from detection_reporter import DetectionReporter
from network_scheduler import NetworkScheduler


class Application:
    def __init__(self):
        self.config = cfg.ConfigParser()
        self.config.read("tf_config.cfg")
        self.execution_path = os.getcwd()
        self.app_type = self.get_config_value("video_input", "dir")
        self.node_id = self.get_config_value("node_id", "unknown")
        self.node_port_num = self.get_config_value("node_port", 4430)

        self.source_type = self.app_type
        if self.source_type == "dir":
            self.source_type = "file"

        self.analyzer = None
        self.observer = None
        self.net_client = None
        self.net_server = None
        self.is_running = True
        self.analyze_enabled = utility.string_to_bool(self.get_config_value("enable_analyze", "True"))
        self.analyze_sync = Condition()
        self.analyze_result = None
        self.network_analyze_enabled = False
        self.network_scheduler = None
        self.local_analyzer_blocked = False
        self.reporter = DetectionReporter(self.config)

    class FileWatcher(PatternMatchingEventHandler):
        patterns = ["*.mp4", "*.mpeg"]

        def __init__(self, callback):
            super().__init__()
            self.callback = callback

        def process(self, event):
            """
            event.event_type
                'modified' | 'created' | 'moved' | 'deleted'
            event.is_directory
                True | False
            event.src_path
                path/to/observed/file
            """
            # the file will be processed there
            print(event.src_path, event.event_type, "is directory:", event.is_directory)  # print now only for degug
            if event.event_type == "created":
                print("add new file", event.src_path)
                self.callback(event.src_path)

        def on_modified(self, event):
            self.process(event)

        def on_created(self, event):
            self.process(event)

        def on_deleted(self, event):
            self.process(event)

        def on_moved(self, event):
            self.process(event)

    def on_frame(self, frame_number, output_array, output_count, returned_frame):
        print("on frame", frame_number)
        pass

    def on_finish(self, output_arrays, count_arrays, average_output_count):
        print("on finished", average_output_count)

        result = messages.Report(instance_id=self.node_id, input_name=self.analyzer.current_infile,
                                 output_name=self.analyzer.current_out_file)
        self.analyzer.clear_current_in_out_source()

        for k in average_output_count.keys():
            result.add_detection(k, average_output_count[k])

        self.analyze_result = result
        self.analyze_sync.acquire()
        self.analyze_sync.notify()
        self.analyze_sync.release()
        self.local_analyzer_blocked = False

        self.reporter.report(result)

    def on_network_finished(self, result):
        print("network analysis finished with : ", result)
        if isinstance(result, messages.Report):
            self.reporter.report(result)

    def get_config_value(self, param, default_value):
        try:
            return self.config["default"][param]
        except IndexError:
            return default_value

    def mode_active(self):
        if self.app_type == "dir":

            self.observer = PollingObserver()
            self.observer.schedule(self.FileWatcher(self.new_data_callback),
                                   self.get_config_value("video_input_src", self.execution_path), recursive=True)
            self.observer.start()
            print("start observing for new files")

        nd_type = self.get_config_value("node_type", "")
        if nd_type == "node":
            pass
        elif nd_type == "master":
            if self.network_analyze_enabled:
                self.network_scheduler = NetworkScheduler()
                nodes = self.get_config_value("nodes_list", "")
                for adr in nodes.split(";"):
                    if adr != "":
                        host,port = adr.split(":")
                        self.network_scheduler.add_node(host, int(port), self.on_network_finished)
                self.network_scheduler.start()
        else:
            print("wrong node type configures : ", nd_type, " -> shutting down application!")
            return

        if self.analyze_enabled:
            self.analyzer = Analyzer(self.config)
            self.analyzer.initialize(self.source_type)
            self.analyzer.set_on_frame(self.on_frame)
            self.analyzer.set_on_finished(self.on_finish)
            self.analyzer.start()
        else:
            print("Analyzer not started...")
            print("start without local analyzing functionality")

        while self.is_running:
            time.sleep(1)

    def mode_train(self):
        # TODO : implement
        pass

    def mode_maintain(self):
        # TODO : implement
        pass

    def new_data_callback(self, data):
        if self.analyze_enabled and self.analyzer is not None and not self.local_analyzer_blocked:
            self.local_analyzer_blocked = True
            self.analyzer.add_data(data)
            return
        if self.network_analyze_enabled and self.network_scheduler is not None:
            m = messages.Detect(data, self.node_id, self.node_port_num)
            self.network_scheduler.add(m)

    def set_analyze_result(self, result):
        self.analyze_result = result

    def get_analyze_result(self):
        return self.analyze_result

    def network_receive_handler(self, message):
        if isinstance(message, dict):
            decoded = True
        else:
            decoded = False
        msg = messages.Message.get_decode_message(message, decoded=decoded)
        msg_type = str(msg.message_type()).lower()
        if msg_type == "detect":
            data = msg.data_source
            if self.analyze_enabled:
                self.analyzer.add_data(data)

                # head, tail = os.path.split(data)
                # vout = os.path.join(self.get_config_value("video_output_dst", self.execution_path),
                #                    self.get_config_value("video_output_prefix", "re_") + tail)
                # print("input : ", data)
                # print("output : ", vout)
                # self.analyzer.analyze_file(data, vout)
                self.analyze_sync.acquire()
                self.analyze_sync.wait()
                self.analyze_sync.release()
                return self.get_analyze_result()
        elif msg_type == "report":
            self.reporter.report(msg)
            pass

        return "error : analyzer disabled or wrong message type"

    def network_respond_handler(self, result):
        if isinstance(result, messages.Message):
            return result.get_data()
        return {"id": self.get_config_value("node_id", "UNKNOWN"), "result": result}

    def network_client_respond_handler(self, message):
        pass

    def run(self):
        # always start the listening server
        port = int(self.get_config_value("node_port", "66666"))
        self.net_server = netserver.NetServer("127.0.0.1", port)
        self.net_server.set_callback(receive_callback=self.network_receive_handler,
                                     respond_callback=self.network_respond_handler)
        self.net_server.start(run_async=True)

        if self.get_config_value("node_type", "") == "master" and self.get_config_value("nodes_list", "") is not "":
            self.network_analyze_enabled = True

        self.reporter.start()

        mode = self.get_config_value("node_mode", "active")
        if mode == "active":
            self.mode_active()
        elif mode == "train":
            self.mode_train()
        elif mode == "maintain":
            self.mode_maintain()
        else:
            print("unknown mode : ", mode, "configured. Application will exit")
