#!/bin/bash

# https://heartbeat.fritz.ai/detecting-objects-in-videos-and-camera-feeds-using-keras-opencv-and-imageai-c869fe1ebcdb

sudo apt-get update
sudo apt-get -y install python3.7 python3-pip

python3.7 -m pip install --upgrade pip

python3.7 -m pip install tensorflow
python3.7 -m pip install numpy
python3.7 -m pip install scipy
python3.7 -m pip install opencv-python
python3.7 -m pip install pillow
python3.7 -m pip install matplotlib
python3.7 -m pip install h5py
python3.7 -m pip install keras
python3.7 -m pip install watchdog

wget https://github.com/OlafenwaMoses/ImageAI/releases/download/2.0.2/imageai-2.0.2-py3-none-any.whl
python3.7 -m pip install imageai-2.0.2-py3-none-any.whl

# get pre-trained model 
wget https://github.com/OlafenwaMoses/ImageAI/releases/download/1.0/yolo.h5
wget https://github.com/OlafenwaMoses/ImageAI/releases/download/1.0/yolo-tiny.h5

# get demo example videos
wget https://github.com/OlafenwaMoses/IntelliP/raw/master/traffic-mini.mp4

# object types:
# person, 
# bicycle, car, motorcycle, airplane,
# bus, train, truck, boat, traffic light, fire hydrant, stop_sign,
# parking meter, bench, bird, cat, dog, horse, sheep, cow, elephant, bear, zebra,
# giraffe, backpack, umbrella, handbag, tie, suitcase, frisbee, skis, snowboard,
# sports ball, kite, baseball bat, baseball glove, skateboard, surfboard, tennis racket,
# bottle, wine glass, cup, fork, knife, spoon, bowl, banana, apple, sandwich, orange,
# broccoli, carrot, hot dog, pizza, donot, cake, chair, couch, potted plant, bed,
# dining table, toilet, tv, laptop, mouse, remote, keyboard, cell phone, microwave,
# oven, toaster, sink, refrigerator, book, clock, vase, scissors, teddy bear, hair dryer,
# toothbrush.