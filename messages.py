import abc
import utility
from enum import Enum


class Message(object, metaclass=abc.ABCMeta):
    _key_massage_type: str = "message_type"

    @abc.abstractmethod
    def message_type(self):
        ...

    @abc.abstractmethod
    def load_data(self, data):
        ...

    @abc.abstractmethod
    def get_data(self):
        ...

    @abc.abstractmethod
    def json_encode(self):
        ...

    @abc.abstractmethod
    def to_string(self):
        ...

    @staticmethod
    @abc.abstractmethod
    def json_decode(data: str):
        ...

    @staticmethod
    def get_message_type(data: dict, on_error_value):
        if Message._key_massage_type in data:
            return data[Message._key_massage_type]
        return on_error_value

    @staticmethod
    def get_decode_message(data, decoded: bool=False):
        if decoded:
            __data = data
        else:
            __data = utility.json_decode(data)

        m_type: str = Message.get_message_type(__data, None)

        if m_type == "Report":
            return Report().load_data(__data)
        if m_type == "Detect":
            return Detect().load_data(__data)
        if m_type == "DefaultResponse":
            return DefaultResponse().load_data(__data)

        return None


class Report(Message):
    __key_id: str = "instance_id"
    __key_detection_objects = "detection_objects"
    __key_detect_obj_type: str = "detection_object_type"
    __key_detect_confidence: float = "detection_confidence"
    __key_input_name: str = "input_name"
    __key_output_name: str = "output_name"

    def __init__(self, instance_id: str=None, input_name: str=None, output_name: str=None):
        self.id: str = instance_id
        self.detection_objects: list = []
        self.input_name: str = input_name
        self.output_name: str = output_name

    def message_type(self):
        return "Report"

    def json_encode(self):
        return utility.json_encode(self.get_data())

    @staticmethod
    def json_decode(data: str):
        js: dict = utility.json_decode(data)
        rp = Report()
        rp.load_data(js)
        return rp

    def to_string(self):
        obj = str("")
        for el in self.detection_objects:
            obj = """%s  %s : %s\n
                            """ % (obj, el[self.__key_detect_obj_type], str(el[self.__key_detect_confidence]))
        message = """Detection Report:\n  Instance: %s\n  Input data: %s\n  Output data: %s\n  Detection: %s
                           """ % (self.id, self.input_name, self.output_name, obj)
        return message

    def load_data(self, data: dict):
        if self.__key_detection_objects in data:
            self.detection_objects = data[self.__key_detection_objects]
        if self.__key_id in data:
            self.id = data[self.__key_id]
        if self.__key_input_name in data:
            self.input_name = data[self.__key_input_name]
        if self.__key_output_name in data:
            self.output_name = data[self.__key_output_name]
        return self

    def get_data(self):
        result = {
            self.__key_output_name: self.output_name,
            self.__key_input_name: self.input_name,
            self.__key_id: self.id,
            self.__key_detection_objects: self.detection_objects,
            self._key_massage_type: "Report"
        }
        return result

    def set_id(self, instance_id: str):
        self.id = instance_id

    def add_detection(self, detection_object: str, detection_confidence: float):
        self.detection_objects.append({
            self.__key_detect_obj_type: detection_object,
            self.__key_detect_confidence: detection_confidence
        })

    def set_input_name(self, input_name: str):
        self.input_name = input_name

    def set_output_name(self, output_name: str):
        self.output_name = output_name

    def get_id(self):
        return self.id

    def get_input_name(self):
        return self.input_name

    def get_output_name(self):
        return self.output_name


class Detect(Message):
    class Type(Enum):
        DIR: str = "dir"
        FILE: str = "file"
        STREAM: str = "stream"

    __key_data_source: str = "data_source"
    __key_data_source_type: str = "data_source_type"
    __key_sender_ip: str = "sender_ip"
    __key_sender_port: str = "sender_port"
    __key_instance_id: str = "instance_id"

    def __init__(self, data_source: str=None, instance_id: str=None, port_number: int=None,
                 data_source_type: Type=Type.DIR):
        self.data_source: str = data_source
        self.data_source_type: Detect.Type = data_source_type
        self.sender: str = utility.get_current_ip()
        self.port_number: int = port_number
        self.id: str = instance_id

    def message_type(self):
        return "Detect"

    def json_encode(self):
        return utility.json_encode(self.get_data())

    @staticmethod
    def json_decode(data: str):
        js = utility.json_decode(data)
        rp = Detect()
        rp.load_data(js)
        return rp

    def to_string(self):
        return self.json_encode()

    def load_data(self, data: dict):
        if self.__key_data_source in data:
            self.data_source = data[self.__key_data_source]
        if self.__key_data_source_type in data:
            self.data_source_type = Detect.Type(data[self.__key_data_source_type])
        if self.__key_sender_ip in data:
            self.sender = data[self.__key_sender_ip]
        if self.__key_sender_port in data:
            self.port_number = data[self.__key_sender_port]
        if self.__key_instance_id in data:
            self.id = data[self.__key_instance_id]
        return self

    def get_data(self):
        result = {
            self.__key_data_source: self.data_source,
            self.__key_data_source_type: self.data_source_type.value,
            self.__key_instance_id: self.id,
            self.__key_sender_ip: self.sender,
            self.__key_sender_port: self.port_number,
            self._key_massage_type: "Detect"
        }
        return result


class DefaultResponse(Message):
    __key_message: str = "message"

    def __init__(self, message="OK"):
        self.message = message

    def message_type(self):
        return "DefaultResponse"

    def load_data(self, data: dict):
        if self.__key_message in data:
            self.message = data[self.__key_message]

        return self

    def get_data(self):
        result = {
            self.__key_message: self.message,
            self._key_massage_type: "DefaultResponse"
        }
        return result

    def json_encode(self):
        return utility.json_encode(self.get_data())

    @staticmethod
    def json_decode(data: str):
        js = utility.json_decode(data)
        rp = DefaultResponse
        rp.load_data(js)
        return rp

    def to_string(self):
        return self.json_encode()
