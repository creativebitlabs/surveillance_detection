import queue
import time
from threading import Thread

import messages
import net_client as netclient


class NetworkScheduler:
    class Node:
        def __init__(self,scheduler, host, port, callback=None):
            self.host = host
            self.port = port
            self.client = None
            self.callback = callback
            self.scheduler = scheduler

        def send(self, message: messages.Message):
            self.client = netclient.NetClient(self.host, self.port, message, self.result)

        def result(self, data):
            self.client = None
            if self.callback is not None:
                self.callback(data)
            print("got response from callback: ", data)
            self.scheduler.reset_blocked_node(self)

    def __init__(self):
        self.nodes = queue.Queue()
        self.blocked_node = [] # queue.Queue()
        self.requests = queue.Queue()
        self.scheduler = None
        self.is_running = True

    def reset_blocked_node(self, node):
        self.blocked_node.remove(node)
        self.nodes.put(node)
        print("reset node : ", node.host, ":", node.port)

    def _scheduler(self):
        while self.is_running:
            if not self.requests.empty() and not self.nodes.empty():
                nd = self.nodes.get()
                msg = self.requests.get()
                self.blocked_node.append(nd)
                nd.send(msg)

            time.sleep(1)

    def start(self):
        self.scheduler = Thread(target=self._scheduler, args=())
        self.scheduler.start()

    def add_node(self, host, port, callback=None):
        self.nodes.put(self.Node(self, host, port, callback))

    def stop(self):
        self.is_running = False
        self.scheduler.join()

    def add(self, message: messages.Message):
        self.requests.put(message)