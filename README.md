# Surveillance

The project shall provide a simple and ready to use machine learning (AI) enhanced application for different surveillance tasks.
Starting from image detection of different objects up to detecting malicious network traffic.

For more details about the project, the status, features and roadmap please visit the [wiki page](https://gitlab.com/creativebitlabs/surveillance_detection/wikis/home).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

A windows or linux machine running 
- Windows 7 or 10 (32/64)
- Debian based distribution (Debian, Ubuntu, Mint and others). Other distributions may work but probably need adoptions

You can also run it on a raspberry pi (recommended raspberry pi 3)

### Installing

On Linux:
- Clone this repo into your project directory
```
git clone https://gitlab.com/creativebitlabs/surveillance_detection.git
```
- Make the setup.sh script executable
```
chmod +x setup.sh
```
- Make the application start script executable
```
chmod +x surveillance
```
- optional : If you are running debian or a debian based distribution where python3.6 or newer is not available. You can add the kali-linux source list repository.
```
echo "deb http://http.kali.org/kali kali-rolling main non-free contrib" | sudo tee -a /etc/apt/sources.list > /dev/null
sudo wget -q -O - https://archive.kali.org/archive-key.asc | apt-key add
```
- run the setup script and follow the on screen instructions
```
sudo ./setup.sh
```

On windows:

- Install Python3.6 or newer and Python pip
- install required packages as the following
```
pip install tensorflow
pip install numpy
pip install scipy
pip install opencv-python
pip install pillow
pip install matplotlib
pip install h5py
pip install keras
pip install watchdog
```

- download and install [imageai](https://github.com/OlafenwaMoses/ImageAI/releases/download/2.0.2/imageai-2.0.2-py3-none-any.whl) wheel
```
pip install imageai-2.0.2-py3-none-any.whl
```

- Clone this repo into your project directory
```
git clone https://gitlab.com/creativebitlabs/surveillance_detection.git
```
- within your project directory donwload a pre-trained [model](https://github.com/OlafenwaMoses/ImageAI/releases/download/1.0/yolo.h5)
- for the first test you can download a example video from the imageai repo. [video](https://github.com/OlafenwaMoses/IntelliP/raw/master/traffic-mini.mp4)


After the the instruction above (for linux and windows) you should have a ready to use setup of the project.

For your first run you can open the tf_config.cfg file and adopt the config parameter to your need. For more details about the config 
parameters please visit the [wiki page](https://gitlab.com/creativebitlabs/surveillance_detection/wikis/home).


## Running the tests

This project is utilizing the python "unittest" framework

Example:
```python
import unittest

class MyTest(unittest.TestCase):
    def test1(self):
        ...
```

You can find all the unittest within the testing directory located in the project's root directory.

To run the unittest's, run the test_main.py script as following:

```
python testing/test_main.py
```

To write and extend unit tests apply the following pattern:

- Within the testing directory create a new python file (It is recommended but not necessary to follow the file name convetion "test_yourtestname.py")
- Within your unit test file apply the following pattern:
```python
# test_my_implementation.py
import unittest
import my_implementation

class UTMyImplementation(unittest.TestCase):
    def test_my_first_test(self):
        do your test stuff
        ...
        self.assertEqual(result1, result2)
        
    def test_my_second_test(self):
        do other stuff here
        ...
        self.assert...
        
        
def get_test_cases():
    return [UTMyImplementation("test_my_first_test"), UTMyImplementation("test_my_second_test")]
```
- Import your unit test class into the test_main.py and add it to the unittest test suite. Important: add the get_test_cases function to all of your unit test files.
- Append your test to the test suite
```python
# test_main.py
import unittest
import test_my_implementation.py as my_impl

def suite():
    __suite = unittest.TestSuite()
    __suite.addTests(my_imp.get_test_cases())
    
    # add other test cases here.

    return __suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())

```


<!--
### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```
-->

## Deployment

The simplest solution is to copy the following files to your live system:
- all .py files within the project root directory
- setup.sh
- surveillance
- tf_config.cfg
- tf_config.cfg.impl
- your already trained ai model (e.g.: my_model.h5)

Edit and change the tf_config.cfg file to your project's need.

Other deployment models/paths will be covered within the [wiki page](https://gitlab.com/creativebitlabs/surveillance_detection/wikis/home).

<!--
## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds
-->
## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/creativebitlabs/surveillance_detection/blob/master/CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/creativebitlabs/surveillance_detection/tags). 

## Authors

* **Christian Poschinger** - *Initial work* - [Creative Bit Labs e.U.](https://gitlab.com/creativebitlabs/)

<!-- See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project. -->

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

This project is based on the following open source projects:
* [ImageAI](https://github.com/OlafenwaMoses/ImageAI/) (MIT License)
* [realpython](https://github.com/realpython/materials/tree/master/python-sockets-tutorial) tutorials. (MIT License)

