import os
from application import Application

'''
documentation

https://imageai.readthedocs.io/en/latest/custom/index.html
https://github.com/OlafenwaMoses/ImageAI/
MIT License

Copyright (c) 2018 MOSES OLAFENWA

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


https://github.com/realpython/materials/tree/master/python-sockets-tutorial
MIT License

Copyright (c) 2018 Real Python

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

TODO : 
    - add network streaming feature as well
    - add setup step to initialize parameters 
    - add error handling for connection problems and recover state (needed for NetworkScheduler)
    - add network triggered stream detection feature 
        -> start network detection from stream with data --> type : stream, source : rtsm://..., reporting : 172.20.20.123:1234, recording : filename or off
        -> respond with success start
        -> detect over time. --> report start of detection , report intermediate update, report end no detection after certain amount time
    - secure and hide password/credentials for service login like mail server 
    
DONE : 
    - add reporting and configuration objects for network transfer.. json, lua, ...
    - add detection reporting engine -> Mail, file, ... what ever
'''


def initial_setup():
    if not os.path.isfile("tf_config.cfg"):
        if input("We are missing the configuration. Do you want to start configuration? Type yes or no :").lower() == "no":
            return False
        with open("tf_config.cfg.impl", 'r') as impl:
            data = impl.read()
            # TODO : ask for values from user
            with open("tf_config.cfg", 'w') as conf:
                conf.write(data)

    return True


if __name__ == "__main__":
    if not initial_setup():
        print("Missing configuration. Shutting down ....")
        exit(-1)

    Application().run()

    exit(0)
