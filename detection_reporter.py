import enum
import queue
import smtplib
import time
from threading import Thread

import messages
import utility


class DetectionReporter:
    class ReportType(enum.Enum):
        EMAIL = "email"
        FILE = "file"

    def __init__(self, config):
        self.config = config
        self.notification_file = "detection.report"
        self. enabled = utility.string_to_bool(self.get_config_value("notification", "false"))
        self.report_type = DetectionReporter.ReportType(self.get_config_value("notification_type", "file"))
        self.datasink: queue.Queue = queue.Queue()
        self.reporter_running = True
        self.scheduler = None

    def start(self):
        if self.enabled:
            self.scheduler = Thread(target=self.__report, args=())
            self.scheduler.start()
        else:
            print("DetectionReport not started. Currently disabled within configuration!")

    def stop(self):
        self.reporter_running = False

    def get_config_value(self, param, default_value):
        try:
            return self.config["default"][param]
        except IndexError:
            return default_value

    def __report_email(self, report: messages.Report):
        cred: str = self.get_config_value("notification_email_from_auth", None)
        if cred is not None:
            cr = cred.split(":")
            user: str =cr[0]
            pwd: str = cr[1]  # TODO : user secure string

        mail_to: list = [self.get_config_value("notification_email_to", None)]
        mail_from: str = self.get_config_value("notification_email_from", None)
        mail_subject: str = self.get_config_value("notification_email_subject", None)

        smtp: str = self.get_config_value("notification_email_smtp_server", None)
        if smtp is not None:
            sm = smtp.split(":")
            smtp_server: str =sm[0]
            smtp_port: int = int(sm[1])

        body = utility.json_encode(report.get_data())

        message = """From: %s\nTo: %s\nSubject: %s\n\n%s
            """ % (mail_from, ", ".join(mail_to), mail_subject, body)
        try:
            # server = smtplib.SMTP_SSL()
            server = smtplib.SMTP()
            server.connect(smtp_server, smtp_port)
            server.ehlo()
            server.starttls()
            server.ehlo()
            server.login(user, pwd)
            server.sendmail(mail_from, mail_to, message)
            server.close()
            print('successfully sent the mail')
        except Exception as ex:
            print("failed to send mail exception: ", ex)

    def __report_file(self, report: messages.Report):
        with open(self.notification_file, 'a') as file:
            file.write(report.to_string())
            file.write("---------------------------------\n")

    def __report(self):
        _rep = None
        if self.report_type == self.ReportType.EMAIL:
            _rep = self.__report_email
        if self.report_type == self.ReportType.FILE:
            _rep = self.__report_file

        while self.reporter_running:
            while not self.datasink.empty():
                _rep(self.datasink.get())
            time.sleep(5)

    def report(self, report: messages.Report):
        if self.enabled:
            self.datasink.put(report)
